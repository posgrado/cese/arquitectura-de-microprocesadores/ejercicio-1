/*=============================================================================
 * Copyright (c) 2020, David Broin <davidmbroin@gmail.com>
 * All rights reserved.
 * License: mit (see LICENSE.txt)
 * Date: 2020/05/30
 *===========================================================================*/

/*=====[Avoid multiple inclusion - begin]====================================*/

#ifndef __EJERCICIO_1_ASM_MODULE_H__
#define __EJERCICIO_1_ASM_MODULE_H__

/*=====[Inclusions of public function dependencies]==========================*/

#include "ejercicio_1.h"

/*=====[C++ - begin]=========================================================*/

#ifdef __cplusplus
extern "C" {
#endif

/*=====[Definition macros of public constants]===============================*/

/*=====[Public function-like macros]=========================================*/

/*=====[Definitions of public data types]====================================*/

/*=====[Prototypes (declarations) of public functions]=======================*/

/*=====[Prototypes (declarations) of public interrupt functions]=============*/

/*=====[External data declaration]===========================================*/

/*=====[external functions declaration]======================================*/

/**
 * @brief C prototype function link with asm_module
 *
 * Automatically when you call and return from function, MCU utilizes these register:
 * 
 * |   C	| CORE    |
 * | ----- 	| ------- |
 * |  r0	|  arg0   |
 * |  r1 	|  arg1   |
 * |  r2 	|  arg2   |
 * |  r3 	|  arg3   |
 * | ----- 	| ------- |
 * |  r0 	| returnL |
 * |  r1 	| returnH |
 *
 * @param arg0
 * @param arg1
 * @param arg2
 * @param arg3
 * @return implicit in r0 when return its 32bits, and r1-r0 when is 64bits
 */
extern void asm_zeros (volatile uint32_t * vector, uint32_t longitud);

/*=====[C++ - end]===========================================================*/

#ifdef __cplusplus
}
#endif

/*=====[Avoid multiple inclusion - end]======================================*/

#endif /* __EJERCICIO_1_ASM_MODULE__ */
