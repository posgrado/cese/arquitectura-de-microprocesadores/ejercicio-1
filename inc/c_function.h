/*=============================================================================
 * Copyright (c) 2020, David Broin <davidmbroin@gmail.com>
 * All rights reserved.
 * License: mit (see LICENSE.txt)
 * Date: 2020/05/30
 *===========================================================================*/

#ifndef __EJERCICIO_1_C_FUNCTION_H__
#define __EJERCICIO_1_C_FUNCTION_H__

#include "ejercicio_1.h"

/**
 * c_function do the same of ASM function for compare purpose
 * 
 * @param * vector vector a llenar con ceros
 * @param longitud
 * @return
 */
void c_zeros (volatile uint32_t * vector, uint32_t longitud);

#endif /* __EJERCICIO_1_C_FUNCTION_H__ */
