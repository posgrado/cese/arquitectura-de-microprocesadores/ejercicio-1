/*=============================================================================
 * Copyright (c) 2020, David Broin <davidmbroin@gmail.com>
 * All rights reserved.
 * License: mit (see LICENSE.txt)
 * Date: 2020/05/30
 *===========================================================================*/

#include "c_function.h"

void c_zeros (volatile uint32_t * vector, uint32_t longitud){
	for (uint32_t var = 0; var < longitud; ++var) {
		vector[var] = 0x00;
	}
}

