/*=============================================================================
 * Copyright (c) 2020, David Broin <davidmbroin@gmail.com>
 * All rights reserved.
 * License: mit (see LICENSE.txt)
 * Date: 2020/05/30
 *===========================================================================*/

/*=====[Inclusions of function dependencies]=================================*/

#include "ejercicio_1.h"
#include "asm_module.h"
#include "c_function.h"
#include "sapi.h"
#include <stdio.h>

/*=====[Definition macros of private constants]==============================*/
#define elementsof(x)  (sizeof(x) / sizeof((x)[0]))

/*=====[Definitions of extern global variables]==============================*/

/*=====[Definitions of public global variables]==============================*/

/*=====[Definitions of private global variables]=============================*/

/*=====[Main function, program entry point after power on or reset]==========*/

int main(void) {

	/*=====[Definitions of private local variables]=============================*/
	volatile uint32_t asm_vector[]={ 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h',
			'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't',
			'u', 'v', 'w', 'x', 'y', 'z' };
	volatile uint32_t c_vector[]={ 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h',
					'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't',
					'u', 'v', 'w', 'x', 'y', 'z' };
	uint32_t cyclesElapsed = 0;

// ----- Setup -----------------------------------
	boardInit();

   // Configura el contador de ciclos con el clock de la EDU-CIAA NXP
   cyclesCounterConfig(EDU_CIAA_NXP_CLOCK_SPEED);

   printf("asm_vector = {");
   for (int var = 0; var < elementsof(asm_vector) - 2; ++var) {
	   printf("%c, ", asm_vector[var]);
   }
   printf("%c }\r\n", asm_vector[elementsof(asm_vector) - 1]);

   // Resetea el contador de ciclos
   cyclesCounterReset();
   //Ejecuta funcion asm
   asm_zeros(asm_vector, elementsof(asm_vector));

   // Guarda en una variable los ciclos leidos
   cyclesElapsed = cyclesCounterRead();
   printf( "La funcion asm se ejecuto en %d ciclos\r\n", cyclesElapsed );

   printf("El resultado fue asm_vector = {");
   for (int var = 0; var < elementsof(asm_vector) - 2; ++var) {
	   printf("0x%02X, ", asm_vector[var]);
   }
   printf("0x%02X }\r\n", asm_vector[elementsof(asm_vector) - 1]);

   printf("c_vector = {");
   for (int var = 0; var < elementsof(c_vector) - 2; ++var) {
	   printf("%c, ", c_vector[var]);
   }
   printf("%c }\r\n", c_vector[elementsof(c_vector) - 1]);

   // Resetea el contador de ciclos
   cyclesCounterReset();
   //Ejecuta funcion C
   c_zeros(c_vector, elementsof(c_vector));

   // Guarda en una variable los ciclos leidos
   cyclesElapsed = cyclesCounterRead();
   printf( "La funcion C se ejecuto en %d ciclos\r\n", cyclesElapsed );

   printf("El resultado fue c_vector = {");
   for (int var = 0; var < elementsof(c_vector) - 2; ++var) {
	   printf("0x%02X, ", c_vector[var]);
   }
   printf("0x%02X }\r\n", c_vector[elementsof(c_vector) - 1]);

// ----- Repeat for ever -------------------------
	while ( true) {
		__WFI(); //wfi
	}

	// YOU NEVER REACH HERE, because this program runs directly or on a
	// microcontroller and is not called by any Operating System, as in the
	// case of a PC program.
	return 0;
}
